// -----------------
// TestLife.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <sstream>   // istringstream
#include <string>    // string

#include "gtest/gtest.h"

#include "Life.hpp"
#include "RunLifeConway.hpp"
#include "RunLifeFredkin.hpp"
#include "RunLifeCell.hpp"

using namespace std;

// --
// IO
// --

TEST(IOFixture, Conway_Parse_Simple)
{
    string input = "4 4\n4\n1 1\n1 2\n2 1\n2 2\n";
    istringstream sin(input);
    Life<ConwayCell> test_conway = get<2>(parse_conway(sin));
    string output = "....\n.**.\n.**.\n....\n";
    ASSERT_EQ(test_conway.print_grid(), output);

}

TEST(IOFixture, Fredkin_Parse_Simple)
{
    string input = "9 9\n3\n4 3\n4 4\n4 5\n";
    istringstream sin(input);
    Life<FredkinCell> test_f = get<2>(parse_fredkin(sin));
    string output = "---------\n---------\n---------\n---------\n---000---\n---------\n---------\n---------\n---------\n";
    ASSERT_EQ(test_f.print_grid(), output);
    ;
}

TEST(IOFixture, Cell_Parse_Simple)
{
    string input = "9 9\n3\n4 3\n4 4\n4 5\n";
    istringstream sin(input);
    Life<Cell> test_c = get<2>(parse_cell(sin));
    string output = "---------\n---------\n---------\n---------\n---000---\n---------\n---------\n---------\n---------\n";
    ASSERT_EQ(test_c.print_grid(), output);
}

// ----------
// ConwayCell
// ----------

TEST(ConwayFixture, Create_Conway_Cell)
{
    ConwayCell test_conway;
    ASSERT_FALSE(test_conway.is_live());
    ConwayCell test_conway2(true);
    ASSERT_TRUE(test_conway2.is_live());
    ConwayCell* test_ptr = &test_conway;
    ASSERT_FALSE(test_ptr->is_live());
    test_ptr = test_conway2.clone();
    ASSERT_TRUE(test_ptr->is_live());
    delete test_ptr;
}

TEST(ConwayFixture, Conway_Logic)
{
    ConwayCell test_conway;
    ASSERT_EQ(test_conway.turn(pair<int, int>(0, 0)), '.');
    ASSERT_EQ(test_conway.turn(pair<int, int>(3, 3)), '.');
    ASSERT_EQ(test_conway.turn(pair<int, int>(4, 0)), '.');
    ASSERT_EQ(test_conway.turn(pair<int, int>(3, 0)), '*');
    ASSERT_EQ(test_conway.turn(pair<int, int>(3, 1)), '.');
    ASSERT_EQ(test_conway.turn(pair<int, int>(0, 0)), '.');
}

// -----------
// FredkinCell
// -----------

TEST(FredkinFixture, Create_Fredkin_Cell)
{
    FredkinCell test_f;
    ASSERT_FALSE(test_f.is_live());
    FredkinCell test_f2(true);
    ASSERT_TRUE(test_f2.is_live());
    FredkinCell* test_ptr = &test_f;
    ASSERT_FALSE(test_ptr->is_live());
    test_ptr = test_f2.clone();
    ASSERT_TRUE(test_ptr->is_live());
    delete test_ptr;
}

TEST(FredkinFixture, Fredkin_Logic)
{
    FredkinCell test_f;
    ASSERT_EQ(test_f.turn(pair<int, int>(0, 0)), '-');
    ASSERT_EQ(test_f.turn(pair<int, int>(3, 3)), '0');
    ASSERT_EQ(test_f.turn(pair<int, int>(4, 1)), '-');
    ASSERT_EQ(test_f.turn(pair<int, int>(3, 0)), '0');
    ASSERT_EQ(test_f.turn(pair<int, int>(1, 0)), '1');
    ASSERT_EQ(test_f.turn(pair<int, int>(0, 0)), '-');
    for(int i = 0; i < 10; ++i)
    {
        test_f.turn(pair<int, int>(3, 0));
    }
    ASSERT_EQ(test_f.turn(pair<int, int>(3, 0)), '+');
}

TEST(FredkinFixture, Monotonic_Aging)
{
    FredkinCell test_f(true);
    ASSERT_EQ(test_f.turn(pair<int, int>(3, 0)), '1');
    ASSERT_EQ(test_f.turn(pair<int, int>(3, 0)), '2');
    ASSERT_EQ(test_f.turn(pair<int, int>(0, 0)), '-');
    ASSERT_EQ(test_f.turn(pair<int, int>(1, 0)), '2');
}

// ----
// Cell
// ----

TEST(CellFixture, Create_Cell)
{
    Cell test_cell;
    ASSERT_FALSE(test_cell.is_live());
    Cell test_cell2(true);
    ASSERT_TRUE(test_cell2.is_live());
    //Copy-Assignment
    test_cell = test_cell2;
    ASSERT_EQ(test_cell.is_live(), test_cell2.is_live());
    ASSERT_NE(test_cell.contents, test_cell2.contents);
    //Copy-Constructor
    Cell test_cell3(test_cell);
    ASSERT_EQ(test_cell.is_live(), test_cell3.is_live());
    ASSERT_NE(test_cell.contents, test_cell2.contents);
}

TEST(CellFixture, Mutate)
{
    Cell test_cell(true);
    char ret;
    //0->1
    ret = test_cell.turn(pair<int,int>(1,0));
    ASSERT_EQ(ret, '1');
    //1->2
    ret = test_cell.turn(pair<int,int>(1,0));
    ASSERT_EQ(ret, '*');
}

// ----
// Life
// ----

TEST(LifeFixture, Life_Conway_Print)
{
    vector<pair<int, int>> input = {
        pair<int, int>(0, 0),
        pair<int, int>(0, 2),
        pair<int, int>(1, 1),
        pair<int, int>(2, 0),
        pair<int, int>(2, 2)
    };
    Life<ConwayCell> test_life(3,3,input);
    string output = "*.*\n.*.\n*.*\n";
    ASSERT_EQ(test_life.print_grid(), output);

    ConwayCell new_cell(true);
    test_life.set_cell(1,2,new_cell);
    output = "*.*\n.**\n*.*\n";
    ASSERT_EQ(test_life.print_grid(), output);
    // clog << test_life.print_grid();
}

TEST(LifeFixture, Life_Conway_Act_Blinker)
{
    vector<pair<int, int>> input = {
        pair<int, int>(0, 1),
        pair<int, int>(1, 1),
        pair<int, int>(2, 1)
    };
    Life<ConwayCell> test_life(3, 3, input);
    string output = ".*.\n.*.\n.*.\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "...\n***\n...\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = ".*.\n.*.\n.*.\n";
    ASSERT_EQ(test_life.print_grid(), output);
}

TEST(LifeFixture, Life_Conway_Act_Lonely)
{
    vector<pair<int, int>> input = {
        pair<int, int>(1, 1)
    };
    Life<ConwayCell> test_life(3, 3, input);
    string output = "...\n.*.\n...\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "...\n...\n...\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "...\n...\n...\n";
    ASSERT_EQ(test_life.print_grid(), output);
}

TEST(LifeFixture, Life_Fredkin_Print)
{
    vector<pair<int, int>> input = {
        pair<int, int>(1, 0),
        pair<int, int>(1, 1),
        pair<int, int>(1, 2)
    };
    Life<FredkinCell> test_life(3, 3, input);
    string output = "---\n000\n---\n";
    ASSERT_EQ(test_life.print_grid(), output);

    FredkinCell new_cell(true);
    test_life.set_cell(0, 1, new_cell);
    output = "-0-\n000\n---\n";
    ASSERT_EQ(test_life.print_grid(), output);
    // clog << test_life.print_grid();
}

TEST(LifeFixture, Life_Fredkin_HR0)
{
    vector<pair<int, int>> input = {
        pair<int, int>(1, 0),
        pair<int, int>(1, 1),
        pair<int, int>(1, 2)
    };
    Life<FredkinCell> test_life(3, 3, input);
    string output = "---\n000\n---\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "000\n1-1\n000\n";
    ASSERT_EQ(test_life.print_grid(), output);
    // clog << test_life.print_grid();
}

TEST(LifeFixture, Life_Cell_Print)
{
    vector<pair<int, int>> input = {
        pair<int, int>(1, 0),
        pair<int, int>(1, 1),
        pair<int, int>(1, 2)
    };
    Life<Cell> test_life(3, 3, input);
    string output = "---\n000\n---\n";
    ASSERT_EQ(test_life.print_grid(), output);
}

TEST(LifeFixture, Life_Cell_HR0)
{
    vector<pair<int, int>> input = {
        pair<int, int>(2, 2),
        pair<int, int>(2, 3),
        pair<int, int>(2, 4)
    };
    Life<Cell> test_life(5, 7, input);
    string output = "-------\n-------\n--000--\n-------\n-------\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "-------\n--000--\n-01-10-\n--000--\n-------\n";
    ASSERT_EQ(test_life.print_grid(), output);
    test_life.act();
    output = "--000--\n-------\n01*-*10\n-------\n--000--\n";
    ASSERT_EQ(test_life.print_grid(), output);
}