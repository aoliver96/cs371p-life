// --------
// Life.hpp
// --------

#ifndef Life_h
#define Life_h

// --------
// includes
// --------

#include <cassert>  // assert
#include <list>     // list
#include <string>   // string
#include <vector>   // vector
#include <utility>  // pair

using namespace std;

// ----
// Life
// ----
template <class CellType>
class Life
{

private:
    int row, col, population;
    string buffer;
    vector<vector<CellType>> grid;

public:
    // ----
    // Life
    // ----
    Life() {}

    // ----
    // Life
    // ----
    /**
    * @param r Row dimension of the Life grid.
    * @param c Column dimension of the Life grid.
    * @param live_cells Vector of <r, c> pairs denoting live cells for initial state.
    * @return A Life object with an r*c grid, populated with live_cells.
    */
    Life(int r, int c, vector<pair<int, int>> &live_cells)
        : grid((r + 2), vector<CellType>(c + 2)) //Grid has two extra rows, columns for easier calculations
    {
        row = r;
        col = c;
        population = 0;
        buffer.reserve((size_t)(r*(c+1)));
        //Initialize living cells
        for (vector<pair<int, int>>::iterator i = live_cells.begin(); i != live_cells.end(); ++i)
        {
            grid[(i->first) + 1][(i->second) + 1] = CellType(true);
        }

        //Initialize output buffer
        for (int i = 0; i < r; ++i)
        {
            for (int j = 0; j < c; ++j)
            {
                buffer += grid[(i + 1)][(j + 1)].get_display();
                if (grid[(i + 1)][(j + 1)].is_live())
                    ++population;
            }
            buffer += '\n';
        }
    }

    // --------
    // set_cell
    // --------
    /**
    * Replaces the cell at position (r, c) with new_cell.
    * @param r Row dimension of the cell.
    * @param c Column dimension of the cell.
    * @param new_cell New CellType object to copy into grid.
    */
    void set_cell(int r, int c, CellType& new_cell)
    {
        // clog << "Printing new_cell character: " << string(1,new_cell.get_display()) << "\n";
        // clog << "Printing grid dimensions (row, col): " << row << ", " << col << "\n";
        // clog << "Printing grid dimensions (grid.size, grid[1].size): " << grid.size() << ", " << grid[1].size() << "\n";
        // clog << "Printing grid[" << r << "]["<<c<<"] character: " << string(1,(grid[r+1][c+1]).get_display()) << "\n";
        assert(r <= row && c <= col);

        if(grid[r + 1][c + 1].is_live() && !new_cell.is_live())
            --population;
        if(!grid[r + 1][c + 1].is_live() && new_cell.is_live())
            ++population;
        grid[r + 1][c + 1] = new_cell;
        buffer[(r*(col+1))+c] = new_cell.get_display();
    }

    // ---
    // act
    // ---
    /**
    * Runs a turn for every cell on the grid.
    */
    void act()
    {
        // Create a grid of neighbor counts (pair<int,int> - cardinal, diagonal)
        vector<vector<pair<int,int>>> neighbors(row+2, vector<pair<int,int>>(col+2, pair<int,int>(0,0)));
        char tmp = ' ';
        bool living = false;

        // For each living cell, update its neighbor's living neighbor count appropriately
        for(int i = 1; i<=row; ++i)
        {
            for(int j = 1; j<=col; ++j)
            {
                if(grid[i][j].is_live())
                {
                    ++(neighbors[i - 1][j - 1].second);
                    ++(neighbors[i - 1][j].first);
                    ++(neighbors[i - 1][j + 1].second);
                    ++(neighbors[i][j - 1].first);
                    ++(neighbors[i][j + 1].first);
                    ++(neighbors[i + 1][j - 1].second);
                    ++(neighbors[i + 1][j].first);
                    ++(neighbors[i + 1][j + 1].second);
                }
            }
        }
        // For each cell with more than 0 neighbors, have it run a turn, and update the buffer
        for (int i = 1; i <= row; ++i)
        {
            for (int j = 1; j <= col; ++j)
            {
                living = grid[i][j].is_live();
                if (neighbors[i][j].first + neighbors[i][j].second > 0)
                {
                    tmp = grid[i][j].turn(neighbors[i][j]);
                    if (!living && grid[i][j].is_live())
                        ++population;
                    if (living && !grid[i][j].is_live())
                        --population;
                    buffer[((i - 1) * (col + 1)) + (j - 1)] = tmp;
                }
                else
                {
                    if(living)
                        --population;
                    buffer[((i - 1) * (col + 1)) + (j - 1)] = grid[i][j].kill();
                }

            }
        }
    }

    // ----------
    // print_grid
    // ----------
    /**
    * @return A string formatted for output representing the grid state
    */
    string print_grid()
    {
        return buffer;
    }

    // --------------
    // get_population
    // --------------
    /**
    * @return An integer representing the number of living cells in the grid
    */
    int get_population()
    {
        return population;
    }
};
#endif // Life_h
