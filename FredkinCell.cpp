// ---------------
// FredkinCell.cpp
// ---------------

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <iostream>
#include <vector>
#include <string>
#include <utility> //pair

#include "FredkinCell.hpp"

using namespace std;

// -----------
// FredkinCell
// -----------
FredkinCell::FredkinCell()
{
    live = false;
    age = 0;
    display = '-';
}

FredkinCell::FredkinCell(bool initial_state)
{
    live = initial_state;
    age = 0;
    if(live)
        display = '0';
    else
        display = '-';
}

// -----
// clone
// -----
FredkinCell *FredkinCell::clone()
{
    return new FredkinCell(*this);
}

// ----
// turn
// ----
char FredkinCell::turn(pair<int, int> neighbors)
{
    int n_count = neighbors.first;
    assert(n_count >= 0 && n_count < 5);
    if(n_count%2 == 0)
        return kill();
    else
    {
        if(live == true)
            ++age;
        live = true;
        // Debug
        // display = '+';
        if (age > 9)
            display = '+';
        else
            display = '0' + age;
        return display;
    }
}

// ----
// kill
// ----
char FredkinCell::kill()
{
    live = false;
    display = '-';
    return display;
}
