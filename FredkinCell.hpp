// ----------------
// FredkinCell.hpp
// ----------------

#ifndef FredkinCell_h
#define FredkinCell_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>

#include "AbstractCell.hpp"

// -----------
// FredkinCell
// -----------

class FredkinCell : public AbstractCell
{

private:
    bool live;
    int age;
    char display;

public:
    // -----------
    // FredkinCell
    // -----------
    /**
    * @return A dead FredkinCell object.
    */
    FredkinCell();

    // -----------
    // FredkinCell
    // -----------
    /**
    * @param initial_state Whether the FredkinCell is alive (true) or dead (false).
    * @return A FredkinCell object with predetermined state.
    */
    FredkinCell(bool initial_state);

    // -----
    // clone
    // -----
    /**
    * @return A pointer to a value-copy of this object.
    */
    FredkinCell *clone();

    // ----
    // turn
    // ----
    /**
    * Runs a turn for the FredkinCell, lives on an odd number of living neighbors, dies otherwise.
    * @param neighbors A pair of values representing the number of living neighbors in the cardinal, and diagonal direcitons.
    * @return A display character representing the state of the cell. ('+', '0'-'9', or '-')
    */
    char turn(std::pair<int, int> neighbors);

    // -------
    // is_live
    // -------
    /**
    * @return Whether or not this FredkinCell is alive.
    */
    bool is_live() {
        return live;
    }

    // -----------
    // get_display
    // -----------
    /**
    * @return The current display character of this FredkinCell.
    */
    char get_display() {
        return display;
    }

    // ----
    // kill
    // ----
    /**
    * Causes the ConwayCell to die with no other computation.
    * @return Returns a dead display character. ('-')
    */
    char kill();
};

#endif // FredkinCell_h