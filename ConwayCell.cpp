// --------------
// ConwayCell.cpp
// --------------

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>  //pair

#include "ConwayCell.hpp"

using namespace std;

// ----------
// ConwayCell
// ----------
ConwayCell::ConwayCell()
{
    live = false;
    display = '.';
}

ConwayCell::ConwayCell(bool initial_state)
{
    live = initial_state;
    if(live)
        display = '*';
    else
        display = '.';
}

// -----
// clone
// -----
ConwayCell* ConwayCell::clone()
{
    return new ConwayCell(*this);
}

// ----
// turn
// ----
char ConwayCell::turn(pair<int, int> neighbors)
{
    int n_count = neighbors.first+neighbors.second;
    assert(n_count >= 0 && n_count < 9);
    if (n_count < 2 || n_count > 3)
    {
        live = false;
        display = '.';
        return '.';
    }
    else
    {
        if(live == false && n_count != 3)
        {
            live = false;
            display = '.';
            return '.';
        }
        live = true;
        display = '*';
        return '*';
    }
}

// ----
// kill
// ----
char ConwayCell::kill()
{
    live = false;
    display = '.';
    return display;
}