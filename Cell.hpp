// --------
// Cell.hpp
// --------

#ifndef Cell_h
#define Cell_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>

#include "AbstractCell.hpp"
#include "ConwayCell.hpp"
#include "FredkinCell.hpp"

#include "gtest/gtest.h"

// ----
// Cell
// ----

class Cell
{

    FRIEND_TEST(CellFixture, Create_Cell);

private:
    AbstractCell* contents;

    // ------
    // mutate
    // ------
    /**
    * Replaces the current contents with a ConwayCell object.
    * @return The display character of the new contents.
    */
    char mutate();

public:

    // ----
    // Cell
    // ----
    /**
    * @return A Cell object with a default initialized FredkinCell.
    */
    Cell();

    // ----
    // Cell
    // ----
    /**
    * @param initial_state The initial state of the cell.
    * @return A Cell object with a FredkinCell of predetermined state.
    */
    Cell(bool initial_state);

    // ----
    // Cell
    // ----
    /**
    * @param old_cell A Cell object to copy the contents of.
    * @return A Cell object with a value-copy of the old cell's contents.
    */
    Cell(const Cell &old_cell);

    // -----
    // ~Cell
    // -----
    /**
    * Destructor which properly frees up the contents of *this.
    */
    ~Cell() {
        delete contents;
    };

    // ---------
    // operator=
    // ---------
    /**
    * Copy-assignment operator which properly value-copies the contents of target cell.
    * @param target A Cell object to copy the contents of.
    * @return A reference to this cell.
    */
    Cell& operator=(const Cell& target);

    // ----
    // turn
    // ----
    /**
    * Runs a turn for the cell contents, mutating if necessary.
    * @param neighbors A pair of values representing the number of living neighbors in the cardinal, and diagonal direcitons.
    * @return The result char of contents.turn().
    */
    char turn(std::pair<int, int> neighbors);

    // -------
    // is_live
    // -------
    /**
    * @return Whether or not the contents of this cell are alive.
    */
    bool is_live() {
        return contents->is_live();
    }

    // -----------
    // get_display
    // -----------
    /**
    * @return The display character of the contents of the cell.
    */
    char get_display() {
        return contents->get_display();
    }

    // ----
    // kill
    // ----
    /**
    * @return The result char from calling kill() on contents.
    */
    char kill() {
        return contents->kill();
    };
};

#endif // Cell_h