.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-10
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
endif

# get git config
config:
	git config -l

# get git log
Life.log:
	git log > Life.log

# compile conway run harness
RunConway: ConwayCell.hpp Life.hpp ConwayCell.cpp RunLifeConway.cpp
	-$(CPPCHECK) ConwayCell.hpp Life.hpp ConwayCell.cpp
	-$(CPPCHECK) RunLifeConway.cpp
	$(CXX) $(CXXFLAGS) ConwayCell.cpp RunLifeConway.cpp -o RunConway
	./RunConway < RunLifeConway.in > RunLifeConway.tmp
	-diff -s RunLifeConway.tmp RunLifeConway.out

# compile fredkin run harness
RunFredkin: FredkinCell.hpp Life.hpp FredkinCell.cpp RunLifeFredkin.cpp
	-$(CPPCHECK) FredkinCell.hpp Life.hpp FredkinCell.cpp
	-$(CPPCHECK) RunLifeFredkin.cpp
	$(CXX) $(CXXFLAGS) FredkinCell.cpp RunLifeFredkin.cpp -o RunFredkin
	./RunFredkin < RunLifeFredkin.in > RunLifeFredkin.tmp
	-diff -s RunLifeFredkin.tmp RunLifeFredkin.out

# compile cell run harness
RunCell: FredkinCell.hpp ConwayCell.hpp AbstractCell.hpp Cell.hpp Life.hpp FredkinCell.cpp ConwayCell.cpp Cell.cpp RunLifeCell.cpp
	-$(CPPCHECK)  FredkinCell.hpp ConwayCell.hpp AbstractCell.hpp Cell.hpp Life.hpp FredkinCell.cpp ConwayCell.cpp Cell.cpp
	-$(CPPCHECK) RunLifeCell.cpp
	$(CXX) $(CXXFLAGS) FredkinCell.cpp ConwayCell.cpp Cell.cpp RunLifeCell.cpp -o RunCell
	./RunCell < RunLifeCell.in > RunLifeCell.tmp
	-diff -s RunLifeCell.tmp RunLifeCell.out

# compile test harness
TestLife: FredkinCell.hpp ConwayCell.hpp AbstractCell.hpp Cell.hpp Life.hpp FredkinCell.cpp ConwayCell.cpp Cell.cpp RunLifeCell.cpp RunLifeFredkin.cpp RunLifeConway.cpp TestLife.cpp
	-$(CPPCHECK) FredkinCell.hpp ConwayCell.hpp AbstractCell.hpp Cell.hpp Life.hpp FredkinCell.cpp ConwayCell.cpp Cell.cpp
	-$(CPPCHECK) RunLifeCell.cpp RunLifeFredkin.cpp RunLifeConway.cpp
	-$(CPPCHECK) TestLife.cpp
	$(CXX) $(CXXFLAGS) FredkinCell.cpp ConwayCell.cpp Cell.cpp RunLifeFredkin.cpp RunLifeConway.cpp RunLifeCell.cpp TestLife.cpp -DNDEBUG -DTesting -o TestLife $(LDFLAGS)

# run/test files, compile with make all
FILES :=		\
    RunConway	\
	RunFredkin	\
	RunCell		\
    TestLife	

# compile all
all: $(FILES)

# check integrity of conway input file
ctd-check-conway:
	$(CHECKTESTDATA) RunLife.ctd RunLifeConway.in

# check integrity of fredkin input file
ctd-check-fredkin:
	$(CHECKTESTDATA) RunLife.ctd RunLifeFredkin.in

# check integrity of cell input file
ctd-check-cell:
	$(CHECKTESTDATA) RunLife.ctd RunLifeCell.in

# generate a random conway input file
ctd-generate-conway:
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeConway.in

# generate a random fredkin input file
ctd-generate-fredkin:
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeFredkin.in

# generate a random cell input file
ctd-generate-cell:
	$(CHECKTESTDATA) -g RunLife.ctd RunLifeCell.in


# execute run harness and diff with expected output
run: RunConway RunFredkin RunCell

# execute test harness
test: TestLife
	$(VALGRIND) ./TestLife
	$(GCOV) -b FredkinCell.cpp ConwayCell.cpp Cell.cpp | grep -B 2 "cpp.gcov"

# clone the Life test repo
Life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-Life-tests.git Life-tests

# test files in the Life test repo
TFILES := `ls Life-tests/*.in`

# execute run harness against a test in Life test repo and diff with expected output
Life-tests/%Conway: RunConway
	./RunConway < $@.in > RunLifeConway.tmp
	-diff RunLifeConway.tmp $@.out

Life-tests/%Fredkin: RunFredkin
	./RunFredkin < $@.in > RunLifeFredkin.tmp
	-diff RunLifeFredkin.tmp $@.out

Life-tests/%Cell: RunCell
	./RunCell < $@.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp $@.out

# execute run harness against all tests in Life test repo and diff with expected output
tests: Life-tests RunConway RunFredkin RunCell
	-for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Cell.cpp
	$(ASTYLE) Cell.hpp
	$(ASTYLE) AbstractCell.hpp
	$(ASTYLE) ConwayCell.cpp
	$(ASTYLE) ConwayCell.hpp
	$(ASTYLE) FredkinCell.cpp
	$(ASTYLE) FredkinCell.hpp
	$(ASTYLE) Life.hpp
	$(ASTYLE) RunLifeCell.cpp
	$(ASTYLE) RunLifeConway.cpp
	$(ASTYLE) RunLifeFredkin.cpp
	$(ASTYLE) TestLife.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Life.hpp ConwayCell.hpp FredkinCell.hpp Cell.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=          \
    .gitignore     \
    .gitlab-ci.yml \
    Life.log    \
    html

# check the existence of check files
check: $(CFILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunConway
	rm -f RunFredkin
	rm -f RunCell
	rm -f TestLife
	rm -f output.txt

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf Life-tests
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version
	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version
	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version
ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version
