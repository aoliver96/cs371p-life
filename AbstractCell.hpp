// ----------------
// AbstractCell.hpp
// ----------------

#ifndef AbstractCell_h
#define AbstractCell_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>

// ------------
// AbstractCell
// ------------

class AbstractCell
{

private:
    bool live;
    char display;

public:

    virtual AbstractCell* clone() = 0;
    virtual char turn(std::pair<int,int> neighbors) = 0;
    virtual char kill() = 0;
    virtual bool is_live() = 0;
    virtual char get_display() = 0;
    virtual ~AbstractCell() {};

};

#endif // AbstractCell_h