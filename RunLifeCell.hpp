// ---------------
// RunLifeCell.hpp
// ---------------

#ifndef RunLifeCell_h
#define RunLifeCell_h

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Cell.hpp"
#include "Life.hpp"

// ----------
// parse_cell
// ----------
/**
    * Parses an input stream into a Life problem
    * @param sin Input stream 
    */
tuple<int, int, Life<Cell>> parse_cell(std::istream &sin);

#endif // RunLifeCell_h
