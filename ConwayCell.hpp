// ----------------
// ConwayCell.hpp
// ----------------

#ifndef ConwayCell_h
#define ConwayCell_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>

#include "AbstractCell.hpp"

// ----------
// ConwayCell
// ----------

class ConwayCell : public AbstractCell
{

private:
    bool live;
    char display;

public:
    // ----------
    // ConwayCell
    // ----------
    /**
    * @return A dead ConwayCell object.
    */
    ConwayCell();

    // -----------
    // ConwayCell
    // -----------
    /**
    * @param initial_state Whether the ConwayCell is alive (true) or dead (false).
    * @return A ConwayCell object with predetermined state.
    */
    ConwayCell(bool initial_state);

    // -----
    // clone
    // -----
    /**
    * @return A pointer to a value-copy of this object.
    */
    ConwayCell* clone();

    // ----
    // turn
    // ----
    /**
    * Runs a turn for the ConwayCell. Becomes live on 3 living neighbors. Dies if less than 2, or more than 3 are alive.
    * @param neighbors A pair of values representing the number of living neighbors in the cardinal, and diagonal direcitons.
    * @return A display character representing the state of the cell. ('*', or '.')
    */
    char turn(std::pair<int,int> neighbors);

    // -------
    // is_live
    // -------
    /**
    * @return Whether or not this ConwayCell is alive.
    */
    bool is_live() {
        return live;
    }

    // -----------
    // get_display
    // -----------
    /**
    * @return The current display character of this ConwayCell.
    */
    char get_display() {
        return display;
    }

    // ----
    // kill
    // ----
    /**
    * Causes the ConwayCell to die with no other computation.
    * @return Returns a dead display character. ('.')
    */
    char kill();
};

#endif // ConwayCell_h