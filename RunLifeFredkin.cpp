// ----------------
// RunLifeFredkin.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>   // string
#include <cstdlib>
#include <vector>
#include <utility>
#include <tuple>

#include "RunLifeFredkin.hpp"

using namespace std;

// ------------
// parse_fredkin
// ------------
tuple<int, int, Life<FredkinCell>> parse_fredkin(istream &sin)
{

    string line;

    // Get World Size
    int r;
    sin >> r;
    int c;
    sin >> c;

    // read the rest of the line off the stream
    getline(sin, line);

    // Get Live cells
    int n;
    sin >> n;
    getline(sin, line);

    vector<pair<int, int>> init_list;

    for (; n > 0; --n)
    {
        int cell_r, cell_c;

        sin >> cell_r;
        sin >> cell_c;

        init_list.push_back(pair<int, int>(cell_r, cell_c));
    }

    return tuple<int, int, Life<FredkinCell>>(r, c, Life<FredkinCell>(r, c, init_list));
}

#ifndef Testing //Compiler ignores this Main when running the test file, and uses GTest Main

// ----
// main
// ----
int main()
{
    string line, output;
    int problems = 0;
    int step, freq;
    tuple<int, int, Life<FredkinCell>> problem_tuple;
    Life<FredkinCell> current_problem;

    // parse problem input for number of Life<Fredkin> instances
    getline(cin, line);
    problems = stoi(line);

    // read the newline off the stream
    getline(cin, line);

    for (; problems > 0; --problems)
    {
        problem_tuple = parse_fredkin(cin);
        current_problem = get<2>(problem_tuple);
        //Get Simulation Parameters

        cin >> step;
        cin >> freq;

        output += "*** Life<FredkinCell> " + to_string(get<0>(problem_tuple)) + "x" + to_string(get<1>(problem_tuple)) + " ***\n\n";
        output += "Generation = 0, Population = " + to_string(current_problem.get_population()) + ".\n";
        output += current_problem.print_grid() + "\n";

        if (step / freq >= 1)
        {
            for (int i = 1; i <= step; ++i)
            {
                current_problem.act();
                if (i % freq == 0)
                {
                    output += "Generation = " + to_string(i) + ", Population = " + to_string(current_problem.get_population()) + ".\n";
                    output += current_problem.print_grid() + "\n";
                }
            }
        }
    }
    output.erase(output.length() - 1);
    cout << output;
    return 0;
}
#endif //Testing
