// -----------------
// RunLifeConway.hpp
// -----------------

#ifndef RunLifeConway_h
#define RunLifeConway_h

// --------
// includes
// --------

#include <tuple>

#include "ConwayCell.hpp"
#include "Life.hpp"

// -------------
// parse_conway
// -------------
/**
    * Parses an input stream into a Life<ConwayCell> problem
    * @param sin Input stream starting at row/column designation
    */
tuple<int, int, Life<ConwayCell>> parse_conway(std::istream &sin);

#endif // RunLifeConway_h
