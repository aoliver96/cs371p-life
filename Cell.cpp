// --------
// Cell.cpp
// --------

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>
#include <utility>

#include "Cell.hpp"

// ------
// mutate
// ------
char Cell::mutate()
{
    delete contents;
    contents = new ConwayCell(true);
    return contents->get_display();
}

// ----
// Cell
// ----
Cell::Cell()
{
    contents = new FredkinCell();
}

Cell::Cell(bool initial_state)
{
    contents = new FredkinCell(initial_state);
}

Cell::Cell(const Cell &old_cell)
{
    contents = old_cell.contents->clone();
}

// ---------
// operator=
// ---------
Cell& Cell::operator=(const Cell& target)
{
    delete contents;
    contents = target.contents->clone();
    return *this;
}

// ----
// turn
// ----
char Cell::turn(std::pair<int, int> neighbors)
{
    char ret = contents->turn(neighbors);
    if(ret == '2')
        return mutate();
    return ret;
}
