// ------------------
// RunLifeFredkin.hpp
// ------------------

#ifndef RunLifeFredkin_h
#define RunLifeFredkin_h

// --------
// includes
// --------

#include <tuple>

#include "FredkinCell.hpp"
#include "Life.hpp"

// -------------
// parse_fredkin
// -------------
/**
    * Parses an input stream into a Life<FredkinCell> problem
    * @param sin Input stream 
    */
tuple<int, int, Life<FredkinCell>> parse_fredkin(std::istream &sin);

#endif // RunLifeFredkin_h
