# CS371p: Object-Oriented Programming Life Repo

* Name: Alex Oliver

* EID: AJO695

* GitLab ID: aoliver96

* HackerRank ID: oliver_alex96

* Git SHA: bc232e69969f9d81ff327ab3b29ad936f17cd157

* GitLab Pipelines: https://gitlab.com/aoliver96/cs371p-life/-/pipelines

* Estimated completion time: 20hr

* Actual completion time: ~15hr

* Comments: